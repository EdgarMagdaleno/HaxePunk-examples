# HaxePunk examples

These are sample applications which demonstrate HaxePunk's features and how to use them.

To build and run an example:

```bash
cd example-classic
openfl test neko
```
